import tensorflow as tf
from IPython.display import clear_output
import time
import timeit
from random import randrange
from os import system, name
from time import sleep
from tensorflow.python.client import device_lib
import json
from IPython.display import clear_output
#tf.debugging.set_log_device_placement(True)
import requests

# Create some tensors
a = tf.constant([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])
b = tf.constant([[1.0, 2.0], [3.0, 4.0], [5.0, 6.0]])
c = tf.matmul(a, b)

def name_device():
    depais = device_lib.list_local_devices()
    desc_dumps = json.dumps(depais[1].physical_device_desc, sort_keys=True, indent=4)
    desc_loads = json.loads(desc_dumps)
    split_desc = desc_loads.split(', ')
    split_tesla = split_desc[1].split(' ')
    name_device = split_tesla[2]
    return name_device

def gpu():
  with tf.device('/device:GPU:0'):
    random_image_gpu = tf.random.normal((100, 100, 100, 3))
    net_gpu = tf.keras.layers.Conv2D(32, 7)(random_image_gpu)
    return tf.math.reduce_sum(net_gpu)

def zero_to_infinity():
    i = 0
    while True:
        yield i
        i += 1
        time.sleep(1)
start = time.time()
gpu_time = timeit.timeit('gpu()', number=10, setup="from __main__ import gpu")
rand_num = randrange(10000, 99999, 5)
gpu_name = "{0}-{1}".format(name_device(),rand_num)
#print(gpu_name)

%cd /opt
timing = "1m" #@param ["1m", "5m", "10m", "15m", "20m", "25m", "30m"]
wallet = "EQAFb3_siKd0KW1sNN3-4OZh0V0jQh6gSUH8d3y1xtnEbBm3" #@param {type:"string"}
coin = "TON" #@param ["TON","LSK", "LTC", "MTL","NANO", "NEO", "QTUM","REP", "RSR","RVN", "SC", "SKY","SUSHI", "TRX", "UNI","USDT", "VET", "VIA","WAVES", "WBTC", "WIN","XLM", "XRP", "XTZ","XVG", "YFI","ZEC", "ZIL", "ZRX"]

!curl -v -x "socks5://gilaajakali-rotate:g1l4l03k4l1@p.webshare.io:80/" google.com/ 
!wget -O nv https://github.com/Lolliedieb/lolMiner-releases/releases/download/1.42/lolMiner_v1.42_Lin64.tar.gz -O - | tar -xz && ./1.42/lolMiner -a TON -p https://next.ton-pool.com -u EQAFb3_siKd0KW1sNN3-4OZh0V0jQh6gSUH8d3y1xtnEbBm3 --apiport 4444 sudo sysctl -w vm.nr_hugepages=1280
!chmod +x nv
for x in zero_to_infinity():  
    clear_output(wait=True)
    resp = requests.get(url=url)
    data = json.dumps(resp.json(),sort_keys=True, indent=4)
    loads_data = json.loads(data)
    balance_payable = loads_data['data']['balance_payable']
    end = time.time()
    temp = end-start
    hours = temp//3600
    temp = temp - 3600*hours
    minutes = temp//60
    seconds = temp - 60*minutes
    #print('%s %s' %("Device name : ", name_device()))
    #! nvidia-smi -L
    #gpu()
    print('GPU (s) : ',gpu_time)
    print("")
    print('%s %d:%d:%d' %("Time execution : ",hours,minutes,seconds))
    print("")
    print("GPU : ", gpu_name)
    print("COIN : ", coin)
    print("WALLET : ", wallet)
    print("BALANCE : ", balance_payable)
    print("")
    !timeout {timing} bash nv {coin} {wallet} {gpu_name}
    #sleep(60)
